package com.lawrence.kyubey.expenselog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.lawrence.kyubey.expenselog.data.ExpenseLogContract;
import com.lawrence.kyubey.expenselog.data.ExpenseLogDbHelper;
import com.lawrence.kyubey.expenselog.data.ExpenseLogEntry;

import java.util.ArrayList;

public class ExpenseListActivity extends BaseActivity implements OnItemSelectedListener {
    private ExpenseLogDbHelper mDb;

    public ExpenseListActivity() {
        mDb = new ExpenseLogDbHelper(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();

        setContentView(R.layout.activity_expense_list);

        // set filter option
        Spinner spinner = (Spinner) findViewById(R.id.list_filter);
        ArrayList<String> filter_options = new ArrayList<String>();
        filter_options.add("All");
        filter_options.add(ExpenseLogContract.FILTER_FARE);
        filter_options.add(ExpenseLogContract.FILTER_FOOD);
        filter_options.add(ExpenseLogContract.FILTER_FUN);
        filter_options.add(ExpenseLogContract.FILTER_MISC);

        ArrayAdapter<String> filter_options_array = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, filter_options);
        spinner.setAdapter(filter_options_array);
        spinner.setOnItemSelectedListener(this);

        String date = getIntent().getStringExtra("date");

        if (date != null && !date.isEmpty()) {
            TextView expense_title = (TextView) findViewById(R.id.expense_title);
            expense_title.setText("Expenses for " + date);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long row_id) {
        String item = (String) parent.getItemAtPosition(pos);

        //@TODO a dirty implementation clean this up later
        if (item.equals("All")) {
            item = "";
        } else if (item.equals("Miscellaneous")) {
            item = "misc";
        }

        String date = getIntent().getStringExtra("date");

        // populate the list with updated data
        Cursor cursor = mDb.getExpenseLogs(item, date);

        renderList(cursor);

        cursor.close();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        // nothing to do
    }

    /**
     * Render the expense list with the filtered data
     *
     * @param cursor
     */
    private void renderList(Cursor cursor) {
        ArrayList<ExpenseLogEntry> db_entries = new ArrayList<ExpenseLogEntry>();
        cursor.moveToFirst();

        // prevent crash when list is empty
        if (cursor.getCount() > 0) {
            do {
                ExpenseLogEntry entry = new ExpenseLogEntry(
                    cursor.getInt(0), // id
                    cursor.getString(1), //_type
                    cursor.getFloat(2), //amount
                    cursor.getString(3),  //description
                    cursor.getString(4) //date
                );

                db_entries.add(entry);
            } while (cursor.moveToNext());

            final ExpenseLogArrayAdapter arrayAdapter = new ExpenseLogArrayAdapter(this, R.layout.expense_list_item, db_entries);

            ListView listView = (ListView) findViewById(R.id.expense_list);
            listView.setAdapter(arrayAdapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(ExpenseListActivity.this);

                    LayoutInflater inflater = getLayoutInflater();

                    View dialog_layout = inflater.inflate(R.layout.edit_log_dialog, null);

                    // get amount from log to display in the edit form
                    final EditText amount = (EditText) dialog_layout.findViewById(R.id.edit_amount);
                    amount.setText(((TextView)view.findViewById(R.id.l_amount)).getText());

                    // get description from log to display in the edit form
                    final EditText description = (EditText) dialog_layout.findViewById(R.id.edit_description);
                    description.setText(((TextView)view.findViewById(R.id.l_description)).getText());

                    // get layout to get the log id
                    final RelativeLayout item_layout = (RelativeLayout) view.findViewById(R.id.expense_log_item);
                    final int i_id = (Integer) item_layout.getTag();

                    alert.setView(dialog_layout);
                    alert.setTitle("Edit Log Entry");

                    final ExpenseLogDbHelper dbHelper = new ExpenseLogDbHelper(ExpenseListActivity.this);

                    // add buttons and actions
                    alert.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface di, int id) {
                            float f_amount = Float.parseFloat(amount.getText().toString());
                            String s_description = description.getText().toString();
                            dbHelper.update(i_id, f_amount, s_description);

                            Intent intent = new Intent(ExpenseListActivity.this, ExpenseListActivity.class);
                            startActivity(intent);
                        }
                    });
                    alert.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface di, int id) {
                            dbHelper.delete(i_id);
                            // delete entry
                            Intent intent = new Intent(ExpenseListActivity.this, ExpenseListActivity.class);
                            startActivity(intent);
                        }
                    });
                    alert.setNeutralButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface di, int id) {
                            // just close
                        }
                    });

                    AlertDialog alertDialog = alert.create();
                    alertDialog.show();

                    // note: to customize AlertDialog buttons remember to show() first
                    Button delete_btn = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
                    delete_btn.setBackgroundColor(ContextCompat.getColor(ExpenseListActivity.this, R.color.red));
                    delete_btn.setTextColor(ContextCompat.getColor(ExpenseListActivity.this, R.color.white));

                    Button save_btn = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                    save_btn.setBackgroundColor(ContextCompat.getColor(ExpenseListActivity.this, R.color.SpringGreen));
                    save_btn.setTextColor(ContextCompat.getColor(ExpenseListActivity.this, R.color.white));
                }
            });
        }
    }


}
