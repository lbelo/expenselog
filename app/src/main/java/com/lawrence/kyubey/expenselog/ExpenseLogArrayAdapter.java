package com.lawrence.kyubey.expenselog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lawrence.kyubey.expenselog.data.ExpenseLogEntry;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by kyubey on 4/13/17.
 */

/**
 * NOTE: A custom ArrayAdapter needs to be implemented because
 * the default ArrayAdapter only accepts a resource with one TextView
 */
public class ExpenseLogArrayAdapter extends ArrayAdapter<ExpenseLogEntry> {

    private Context context;
    private ArrayList<ExpenseLogEntry> expenseLogArrayList;

    public ExpenseLogArrayAdapter(Context context, int resource, ArrayList<ExpenseLogEntry> expenseLogArrayList) {
        super(context, resource, expenseLogArrayList);

        this.context = context;
        this.expenseLogArrayList = expenseLogArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.expense_list_item, null);

        ExpenseLogEntry logEntry = expenseLogArrayList.get(position);
        TextView expense_type = (TextView) rowView.findViewById(R.id.l_type);
        expense_type.setText(logEntry.type);

        DecimalFormat decimalFormatter = new DecimalFormat("#,###.00");
        String formatted_amount = decimalFormatter.format(logEntry.amount);

        TextView expense_amount = (TextView) rowView.findViewById(R.id.l_amount);
        expense_amount.setText(formatted_amount);

        TextView expense_desc = (TextView) rowView.findViewById(R.id.l_description);
        expense_desc.setText(logEntry.description);

        TextView expense_date = (TextView) rowView.findViewById(R.id.l_date);
        expense_date.setText(logEntry.date);

        RelativeLayout item_layout = (RelativeLayout) rowView.findViewById(R.id.expense_log_item);
        item_layout.setTag(logEntry.id);

        return rowView;
    }
}
