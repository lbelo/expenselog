package com.lawrence.kyubey.expenselog.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by kyubey on 5/3/17.
 */

public class BaseDbHelper extends SQLiteOpenHelper {
    public BaseDbHelper(Context context) {
        super(context, ExpenseLogContract.DATABASE_NAME, null, ExpenseLogContract.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // create the table
        String sql = "CREATE TABLE " + ExpenseLogContract.EXPENSE_LOG + " (" +
                "id          INTEGER PRIMARY KEY AUTOINCREMENT," +
                "type        STRING  NOT NULL," +
                "amount      REAL    NOT NULL," +
                "description TEXT," +
                "date_added  TIME    DEFAULT (datetime(CURRENT_TIMESTAMP,'localtime')))";

        db.execSQL(sql);


        // create archiving table for deleted entries
        String sql2 = "CREATE TABLE " + ExpenseLogContract.EXPENSE_LOG_ARCHIVE + " (" +
                "id          INTEGER PRIMARY KEY AUTOINCREMENT," +
                "type        STRING  NOT NULL," +
                "amount      REAL    NOT NULL," +
                "description TEXT," +
                "date_added  TIME    DEFAULT NULL)";

        db.execSQL(sql2);


        // create settings table
        String sql3 = "CREATE TABLE " + ExpenseLogContract.SETTINGS + " (" +
                "id      INTEGER PRIMARY KEY AUTOINCREMENT," +
                "type    STRING  NOT NULL," +  // setting category
                "name    STRING  NOT NULL," +
                "value   REAL    NOT NULL)";

        db.execSQL(sql3);

        // daily setting
        db.execSQL("INSERT INTO " + ExpenseLogContract.SETTINGS + " (type, name, value) VALUES ('day', 'sunday', 0)");
        db.execSQL("INSERT INTO " + ExpenseLogContract.SETTINGS + " (type, name, value) VALUES ('day', 'monday', 0)");
        db.execSQL("INSERT INTO " + ExpenseLogContract.SETTINGS + " (type, name, value) VALUES ('day', 'tuesday', 0)");
        db.execSQL("INSERT INTO " + ExpenseLogContract.SETTINGS + " (type, name, value) VALUES ('day', 'wednesday', 0)");
        db.execSQL("INSERT INTO " + ExpenseLogContract.SETTINGS + " (type, name, value) VALUES ('day', 'thursday', 0)");
        db.execSQL("INSERT INTO " + ExpenseLogContract.SETTINGS + " (type, name, value) VALUES ('day', 'friday', 0)");
        db.execSQL("INSERT INTO " + ExpenseLogContract.SETTINGS + " (type, name, value) VALUES ('day', 'saturday', 0)");

        // weekly setting
        db.execSQL("INSERT INTO " + ExpenseLogContract.SETTINGS + " (type, name, value) VALUES ('week', 'total', 0)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
