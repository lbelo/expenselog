package com.lawrence.kyubey.expenselog;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = new MenuInflater(this);
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     *  Show expense logs as list
     *  NOTE: function signature for app bar menu must be: void myfunc(MenuItem)
     */
    public void showLogs(MenuItem item) {
        Intent logIntent = new Intent(this, ExpenseListActivity.class);
        startActivity(logIntent);
    }

    /**
     * Opens the Settings Activity
     * @param item
     */
    public void showSettings(MenuItem item) {
        Intent settingIntent = new Intent(this, SettingsActivity.class);
        startActivity(settingIntent);
    }

    /**
     * Opens the Status Activity
     * @param item
     */
    public void showStatus(MenuItem item) {
        Intent settingIntent = new Intent(this, StatusActivity.class);
        startActivity(settingIntent);
    }

    /**
     * Opens the Main Activity
     * @param item
     */
    public void showMain(MenuItem item) {
        Intent logIntent = new Intent(this, MainActivity.class);
        startActivity(logIntent);
    }

    /**
     *  Show daily totals log as list
     *  NOTE: function signature for app bar menu must be: void myfunc(MenuItem)
     */
    public void showSummaries(MenuItem item) {
        Intent dailyTotals = new Intent(this, SummaryTotals.class);
        startActivity(dailyTotals);
    }
}
