package com.lawrence.kyubey.expenselog.data;

/**
 * Created by kyubey on 4/13/17.
 */

public class ExpenseLogEntry {
    public int id;
    public String type;
    public float amount;
    public String description;
    public String date;

    public ExpenseLogEntry(int id, String type, float amount, String description, String date) {
        this.id = id;
        this.type = type;
        this.amount = amount;
        this.description = description;
        this.date = date;
    }
}
