package com.lawrence.kyubey.expenselog;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.TextView;

import com.lawrence.kyubey.expenselog.data.SettingsDbHelper;

public class SettingsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();

        setContentView(R.layout.activity_settings);

        // daily setting
        setFocusListener(R.id.sun_limit, "day", "sunday");
        setFocusListener(R.id.mon_limit, "day", "monday");
        setFocusListener(R.id.tue_limit, "day", "tuesday");
        setFocusListener(R.id.wed_limit, "day", "wednesday");
        setFocusListener(R.id.thu_limit, "day", "thursday");
        setFocusListener(R.id.fri_limit, "day", "friday");
        setFocusListener(R.id.sat_limit, "day", "saturday");

        // week setting
        setFocusListener(R.id.week_limit, "week", "total");
    }

    /**
     * Set focus listener to trigger save when the setting is changed
     *
     * @param id View ID
     * @param type type of setting
     * @param name setting name
     */
    private void setFocusListener(int id, final String type, final String name) {
        final SettingsDbHelper dbHelper = new SettingsDbHelper(this);

        final EditText et_setting = (EditText) findViewById(id);
        et_setting.setHint("200");

        // show saved value
        if (type.equals("week")) {
            et_setting.setText(dbHelper.getWeekSetting() + "");
        } else {
            et_setting.setText(dbHelper.getDaySetting(name) + "");
        }

        et_setting.setOnFocusChangeListener(new OnFocusChangeListener() {
               @Override
               public void onFocusChange(View view, boolean b) {
                   EditText editText = (EditText) view;
                   String value = editText.getText().toString();
                   if (value.equals("")) {
                       value = "0";
                   }

                   dbHelper.update(type, name, Float.valueOf(value));
               }
           }
        );
    }
}
