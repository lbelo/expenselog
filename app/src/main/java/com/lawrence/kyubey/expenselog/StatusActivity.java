package com.lawrence.kyubey.expenselog;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.DefaultAxisValueFormatter;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.lawrence.kyubey.expenselog.data.ExpenseLogDbHelper;
import com.lawrence.kyubey.expenselog.data.SettingsDbHelper;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class StatusActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }
    @Override
    protected void onStart() {
        super.onStart();
        setContentView(R.layout.activity_status);

        SettingsDbHelper settingsDbHelper = new SettingsDbHelper(this);

        TextView todays_date = (TextView) findViewById(R.id.today_date);
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE MMMM d, yyyy");
        todays_date.setText(formatter.format(date).toString());

        ExpenseLogDbHelper expense_log = new ExpenseLogDbHelper(this);
        float amount = expense_log.getTodaysTotal();
        DecimalFormat decimalFormatter = new DecimalFormat("#,###.00");
        String formatted_amount = decimalFormatter.format(amount);

        TextView todays_total = (TextView) findViewById(R.id.day_total);
        todays_total.setText(formatted_amount);

        SimpleDateFormat formatter_day = new SimpleDateFormat("EEEE");
        float today_setting = settingsDbHelper.getDaySetting(formatter_day.format(date).toString().toLowerCase());

        TextView todays_remaining = (TextView) findViewById(R.id.day_remaining);
        todays_remaining.setText(decimalFormatter.format(today_setting - amount));

        float week_amount = expense_log.getThisWeeksTotal();
        String formatted_week_amount = decimalFormatter.format(week_amount);

        TextView week_total = (TextView) findViewById(R.id.week_total);
        week_total.setText(formatted_week_amount);

        float week_setting = settingsDbHelper.getWeekSetting();

        TextView week_remaining = (TextView) findViewById(R.id.week_remaining);
        week_remaining.setText(decimalFormatter.format(week_setting - week_amount));

        renderWeekBudgetChart();
        renderWeekChart();
    }

    /**
     * Renders weekly expense bar chart
     */
    private void renderWeekChart() {
        BarChart barChart = (BarChart) findViewById(R.id.chart);
        //barChart.getXAxis().setEnabled(true);
        barChart.getDescription().setEnabled(false);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                String[] days = new String[]{"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"};
                return days[(int)value];
            }
        });

        // get budget settings
        SettingsDbHelper sDbHelper = new SettingsDbHelper(this);
        float budgets[] = sDbHelper.getDailyBudget();

        // prepare spending budgetted data
        ArrayList<BarEntry> values1 = new ArrayList<>();
        values1.add(new BarEntry(0, budgets[0])); //sunday
        values1.add(new BarEntry(1, budgets[1])); //monday
        values1.add(new BarEntry(2, budgets[2])); //tuesday
        values1.add(new BarEntry(3, budgets[3])); //wednesday
        values1.add(new BarEntry(4, budgets[4])); //thursday
        values1.add(new BarEntry(5, budgets[5])); //friday
        values1.add(new BarEntry(6, budgets[6])); //saturday

        // put data in a set
        BarDataSet budget = new BarDataSet(values1, "Budget");
        budget.setColor(ContextCompat.getColor(this, R.color.SpringGreen));
        budget.setDrawValues(false);

        // get spending data
        ExpenseLogDbHelper dbHelper = new ExpenseLogDbHelper(this);
        float spending[] = dbHelper.getCurrentWeekSpendings();

        // prepare spending  data
        ArrayList<BarEntry> values2 = new ArrayList<>();
        values2.add(new BarEntry(0, spending[0])); //sunday
        values2.add(new BarEntry(1, spending[1])); //monday
        values2.add(new BarEntry(2, spending[2])); //tuesday
        values2.add(new BarEntry(3, spending[3])); //wednesday
        values2.add(new BarEntry(4, spending[4])); //thursday
        values2.add(new BarEntry(5, spending[5])); //friday
        values2.add(new BarEntry(6, spending[6])); //saturday

        // put data in a set
        BarDataSet spent = new BarDataSet(values2, "Spent");
        spent.setColor(ContextCompat.getColor(this, R.color.red));

        // create a data object with the datasets
        BarData data = new BarData(budget, spent);

        // set data
        barChart.setData(data);

        // chart customization
        barChart.setFitBars(true); // make the x-axis fit exactly all bars
        barChart.getAxisRight().setDrawLabels(false); //labels on left only
        barChart.getAxisRight().setDrawGridLines(false);

        barChart.getAxisLeft().setAxisMinimum(0);
        barChart.getAxisLeft().setDrawGridLines(false);

        barChart.invalidate(); // refresh
    }

    /**
     * Horizontal bar chart for weekly budget
     */
    private void renderWeekBudgetChart() {
        HorizontalBarChart barChart = (HorizontalBarChart) findViewById(R.id.week_chart);
        //barChart.getXAxis().setEnabled(true);
        barChart.getDescription().setEnabled(false);

        // get budget settings
        SettingsDbHelper sDbHelper = new SettingsDbHelper(this);
        float weeklyBudget = sDbHelper.getWeekSetting();

        // prepare spending budgetted data
        ArrayList<BarEntry> values1 = new ArrayList<>();
        values1.add(new BarEntry(0, weeklyBudget)); //the weeks budget

        // put data in a set
        BarDataSet budget = new BarDataSet(values1, "Budget");
        budget.setColor(ContextCompat.getColor(this, R.color.SpringGreen));

        // get spending data
        ExpenseLogDbHelper dbHelper = new ExpenseLogDbHelper(this);
        float spending = dbHelper.getThisWeeksTotal();

        // prepare spending  data
        ArrayList<BarEntry> values2 = new ArrayList<>();
        values2.add(new BarEntry(0, spending)); //the weeks spendings

        // put data in a set
        BarDataSet spent = new BarDataSet(values2, "Spent");
        spent.setColor(ContextCompat.getColor(this, R.color.red));

        // create a data object with the datasets
        BarData data = new BarData(budget, spent);

        // set data
        barChart.setData(data);

        // disable labels
        barChart.getXAxis().setDrawLabels(false);
        barChart.getAxisRight().setDrawLabels(false);
        barChart.getAxisRight().setAxisMinimum(0);
        barChart.getAxisRight().setDrawGridLines(false);

        barChart.getAxisLeft().setSpaceTop(0);
        barChart.getAxisLeft().setDrawLabels(false);
        barChart.getAxisLeft().setAxisMinimum(0);
        barChart.getAxisLeft().setDrawGridLines(false);

        barChart.invalidate(); // refresh
    }
}
