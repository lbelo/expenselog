package com.lawrence.kyubey.expenselog;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lawrence.kyubey.expenselog.data.ExpenseLogDbHelper;

/** TODO:
 * Change the default icon (done)
 * Ideas
 * - expense limit - over expense warning - need to think about this more
 * - expense list filters by type, date (done)
 * - reports e.g biggest expense of the day/week/month
 * - daily, weekly, monthly charts
 * - export data to csv - not as easy as it sounds
 * - customizable buttons/categories ??
 */

public class MainActivity extends BaseActivity implements OnClickListener {

    private ExpenseLogDbHelper mDb;

    public MainActivity() {
        mDb = new ExpenseLogDbHelper(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onClick(View view) {
        String button_clicked = view.getTag().toString();

        openExpenseInputForm(button_clicked);
    }

    /**
     * Saves expense data to database
     */
    private void saveExpenseData(String type, float amount, String description) {
        mDb.insert(type, amount, description);
    }

    /**
     *
     */
    private void openExpenseInputForm(final String type) {
        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);

        LayoutInflater inflater = getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.edit_log_dialog, null);

        // Initialize form fields
        final EditText amount = (EditText) dialog_layout.findViewById(R.id.edit_amount);
        final EditText description = (EditText) dialog_layout.findViewById(R.id.edit_description);

        alert.setView(dialog_layout);
        alert.setTitle("How much did you spend?");

        final ExpenseLogDbHelper dbHelper = new ExpenseLogDbHelper(MainActivity.this);

        // add buttons and actions
        alert.setPositiveButton("Save", null);

        alert.setNeutralButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface di, int id) {
                // just close
            }
        });

        // note: to customize AlertDialog buttons remember to show() first
        AlertDialog alertDialog = alert.create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(final DialogInterface dialog) {
                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        float f_amount = amount.getText().toString().isEmpty() ? 0 : Float.parseFloat(amount.getText().toString());
                        String s_description = description.getText().toString();

                        if (f_amount > 0) {
                            // save data
                            saveExpenseData(type, f_amount, s_description);

                            //Dismiss once everything is OK.
                            dialog.dismiss();
                        } else {
                            //do nothing
                        }
                    }
                });
            }
        });

        alertDialog.show();

        Button save_btn = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        save_btn.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.SpringGreen));
        save_btn.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.white));
    }
}
