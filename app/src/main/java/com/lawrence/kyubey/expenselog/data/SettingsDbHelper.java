package com.lawrence.kyubey.expenselog.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

/**
 * Created by kyubey on 4/19/17.
 */

public class SettingsDbHelper extends BaseDbHelper {
    public SettingsDbHelper(Context context) {
        super(context);
    }

    /**
     * Insert settings data to database
     */
    public void insert() {
        //never insert values are pre-set
    }

    /**
     * Update settings data
     *
     * @param type
     * @param name
     * @param value
     */
    public void update(String type, String name, float value) {
        SQLiteDatabase db = getWritableDatabase();

        String sql = "UPDATE " + ExpenseLogContract.SETTINGS + " SET value = ? WHERE name = ? AND type = ?";
        SQLiteStatement statement = db.compileStatement(sql);

        statement.bindDouble(1, value);
        statement.bindString(2, name);
        statement.bindString(3, type);

        statement.execute();
    }

    /**
     * Get setting for a specified day
     *
     * @return setting for the day
     */
    public float getDaySetting(String day) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = null;
        float budgets[] = new float[7];

        String[] projection = {"value"}; //setting_name, value
        cursor = db.query(
                ExpenseLogContract.SETTINGS,
                projection, //columns
                "type = 'day' AND name = ?", //where
                new String[]{day}, //where args
                null, //group by
                null, //having
                null, //order by
                "1" //limit
        );

        cursor.moveToFirst();
        float value = cursor.getFloat(0); //value
        cursor.close();

        return value;
    }

    /**
     * Get setting for the week
     *
     * @return setting for the week
     */
    public float getWeekSetting() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = null;
        float budgets[] = new float[7];

        String[] projection = {"value"}; //setting_name, value
        cursor = db.query(
                ExpenseLogContract.SETTINGS,
                projection, //columns
                "type = 'week' AND name = 'total'", //where
                null, //where args
                null, //group by
                null, //having
                null, //order by
                "1" //limit
        );

        cursor.moveToFirst();
        float value = cursor.getFloat(0); //value
        cursor.close();

        return value;
    }

    /**
     * Get budget per day based on the user defined settings
     *
     * @return array of budgets per day (Sunday begins at index 0)
     */
    public float[] getDailyBudget() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = null;
        float budgets[] = new float[7];

        String[] projection = {"name", "value"}; //setting_name, value
        cursor = db.query(
            ExpenseLogContract.SETTINGS,
            projection, //columns
            "type = 'day'", //where
            null, //where args
            null, //group by
            null, //having
            null, //order by
            null //limit
        );

        cursor.moveToFirst();

        // TODO fix this, for now it is assumed the days will
        // be in order from sunday to saturday
        int count = 0;
        do {
            String day = cursor.getString(0); //day
            float value = cursor.getFloat(1); //value

            budgets[count] = value;
            count++;
        } while (cursor.moveToNext());

        cursor.close();

        return budgets;
    }
}
