package com.lawrence.kyubey.expenselog.data;

/**
 * Created by kyubey on 4/12/17.
 */

public final class ExpenseLogContract {
    public static final String DATABASE_NAME = "expense.db";

    /**
     * Ver.1: expense_log table
     * Ver.2: settings_log table added
     */
    public static final int DATABASE_VERSION = 4;

    // tables
    public static final String EXPENSE_LOG = "expense_log";
    public static final String SETTINGS = "settings";
    public static final String EXPENSE_LOG_ARCHIVE = "archives";

    // filters
    public static final String FILTER_FARE = "Fare";
    public static final String FILTER_FOOD = "Food";
    public static final String FILTER_FUN  = "Fun";
    public static final String FILTER_MISC = "Miscellaneous";
}
