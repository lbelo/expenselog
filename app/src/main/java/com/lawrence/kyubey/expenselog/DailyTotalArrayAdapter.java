package com.lawrence.kyubey.expenselog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.lawrence.kyubey.expenselog.data.SummaryTotalEntry;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by kyubey on 6/10/17.
 */

public class DailyTotalArrayAdapter extends ArrayAdapter<SummaryTotalEntry> {

    private Context context;
    private ArrayList<SummaryTotalEntry> summaryTotalEntryArrayList;

    public DailyTotalArrayAdapter(Context context, int resource, ArrayList<SummaryTotalEntry> summaryTotalEntryArrayList) {
        super(context, resource, summaryTotalEntryArrayList);

        this.context = context;
        this.summaryTotalEntryArrayList = summaryTotalEntryArrayList;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.daily_total_list_item, null);

        SummaryTotalEntry logEntry = summaryTotalEntryArrayList.get(position);
        TextView day_date = (TextView) rowView.findViewById(R.id.dt_date);
        day_date.setText(logEntry.date);

        DecimalFormat decimalFormatter = new DecimalFormat("#,###.00");
        String formatted_amount = decimalFormatter.format(logEntry.amount);

        TextView total_amount = (TextView) rowView.findViewById(R.id.dt_amount);
        total_amount.setText(formatted_amount);

        return rowView;
    }
}
