package com.lawrence.kyubey.expenselog.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by kyubey on 4/12/17.
 */

public class ExpenseLogDbHelper extends BaseDbHelper {
    public ExpenseLogDbHelper(Context context) {
        super(context);
    }

    /**
     * Insert expense data to database
     *
     * @param type
     * @param amount
     * @param description
     */
    public void insert(String type, float amount, String description) {
        SQLiteDatabase db = getWritableDatabase();

        String sql = "INSERT INTO " + ExpenseLogContract.EXPENSE_LOG + " (type, amount, description) VALUES (?, ?, ?)";
        SQLiteStatement statement = db.compileStatement(sql);

        statement.bindString(1, type);
        statement.bindDouble(2, amount);
        statement.bindString(3, description);

        statement.executeInsert();
    }

    /**
     * Update expense data
     *
     * @param id
     * @param amount
     * @param description
     */
    public void update(int id, float amount, String description) {
        SQLiteDatabase db = getWritableDatabase();

        String sql = "UPDATE " + ExpenseLogContract.EXPENSE_LOG + " SET amount = ?, description = ? WHERE id = ?";
        SQLiteStatement statement = db.compileStatement(sql);

        statement.bindDouble(1, amount);
        statement.bindString(2, description);
        statement.bindLong(3, id);

        statement.executeUpdateDelete();
    }

    /**
     * Delete expense data
     *
     * @param id
     */
    public void delete(int id) {
        ExpenseLogEntry entry = getExpenseLog(id);

        SQLiteDatabase write_db = getWritableDatabase();

        // save log to archive
        String sql = "INSERT INTO " + ExpenseLogContract.EXPENSE_LOG_ARCHIVE + " (type, amount, description, date_added) VALUES (?, ?, ?, ?)";
        SQLiteStatement archive_statement = write_db.compileStatement(sql);

        archive_statement.bindString(1, entry.type);
        archive_statement.bindDouble(2, entry.amount);
        archive_statement.bindString(3, entry.description);
        archive_statement.bindString(4, entry.date);

        archive_statement.executeInsert();


        // delete log from main table
        sql = "DELETE FROM " + ExpenseLogContract.EXPENSE_LOG + " WHERE id = ?";
        SQLiteStatement delete_statement = write_db.compileStatement(sql);

        delete_statement.bindLong(1, id);

        delete_statement.executeUpdateDelete();
    }

    /**
     * Get expense log
     *
     * @param id
     * @return expense_log
     */
    public ExpenseLogEntry getExpenseLog(int id) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = null;

        String[] projection = {"id", "type", "amount", "description", "date_added"};
        cursor = db.query(
                ExpenseLogContract.EXPENSE_LOG,
                projection, //columns
                "id = " + id, //where
                null, //where args
                null, //group by
                null, //having
                null, //order by
                "1" //limit
        );

        cursor.moveToFirst();
        ExpenseLogEntry entry = new ExpenseLogEntry(
                cursor.getInt(0),
                cursor.getString(1),
                cursor.getFloat(2),
                cursor.getString(3),
                cursor.getString(4)
        );
        cursor.close();

        return entry;
    }

    /**
     * Get expense logs
     *
     * @param filter
     * @param date
     * @return expense_logs
     */
    public Cursor getExpenseLogs(String filter, String date) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = null;
        String where = null;
        ArrayList<String> args = new ArrayList<String>();

        if (filter != null && !filter.equals("")) {
            where = "type = ?";
            args.add(filter.toLowerCase());
        }

         if (date != null) {
            where = (where != null) ? where + " and date(date_added) = ?" : "date(date_added) = ?";
            args.add(date);
        }


        String[] projection = {"id", "type", "amount", "description", "strftime('%Y-%m-%d %H:%M', date_added) as date_added"};
        cursor = db.query(
            ExpenseLogContract.EXPENSE_LOG,
            projection, //columns
            where, //where
            args.size() > 0 ? args.toArray(new String[0]) : null, //where args
            null, //group by
            null, //having
            "date_added DESC", //order by
            null //limit
        );

        return cursor;
    }

    /**
     * Get running total of the day
     *
     * @return total
     */
    public float getTodaysTotal() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = null;

        String[] projection = {"sum(amount) as total"};
        cursor = db.query(
            ExpenseLogContract.EXPENSE_LOG,
            projection, //columns
            "date(date_added) = date('now')", //where
            null, //where args
            null, //group by
            null, //having
            null, //order by
            "1" //limit
        );

        cursor.moveToFirst();
        float total = cursor.getFloat(0);
        cursor.close();

        return total;
    }

    /**
     * Get total expenses for the current week beginning monday
     *
     * @return total
     */
    public float getThisWeeksTotal() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = null;

        String last_sunday = getDateLastSunday();

        String[] projection = {"sum(amount) as total"};
        cursor = db.query(
                ExpenseLogContract.EXPENSE_LOG,
                projection, //columns
                "date(date_added) >= date(?)", //where
                new String[]{last_sunday}, //where args
                null, //group by
                null, //having
                null, //order by
                null //limit
        );

        cursor.moveToFirst();
        float total = cursor.getFloat(0);
        cursor.close();

        return total;
    }

    /**
     * Get list of expenses per day for the current week
     *
     * @return array of expenses per day (Sunday begins at index 0)
     */
    public float[] getCurrentWeekSpendings() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = null;
        float spending[] = new float[7];

        //initialize values
        spending[0] = 0;
        spending[1] = 0;
        spending[2] = 0;
        spending[3] = 0;
        spending[4] = 0;
        spending[5] = 0;
        spending[6] = 0;

        String last_sunday = getDateLastSunday();

        String[] projection = {"strftime('%w', date_added) as date_added", "sum(amount) as total"};
        cursor = db.query(
                ExpenseLogContract.EXPENSE_LOG,
                projection, //columns
                "date(date_added) >= date(?)", //where
                new String[]{last_sunday}, //where args
                "strftime('%Y-%m-%d', date_added)", //group by
                null, //having
                "date_added", //order by
                null //limit
        );

        cursor.moveToFirst();

        if (cursor.getCount() > 0) {
            do {
                int day = cursor.getInt(0); //day
                float day_total = cursor.getFloat(1); //day total

                spending[day] = day_total;
            } while (cursor.moveToNext());
        }

        cursor.close();

        return spending;
    }

    /**
     * Returns an array of strings showing the spending total of each day
     *
     * @return total spending per day
     */
    public Cursor getTotalsPerDay() {
       SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = null;

        String[] projection = {"strftime('%Y-%m-%d', date_added) as date", "sum(amount) as total"};
        cursor = db.query(
                ExpenseLogContract.EXPENSE_LOG,
                projection, //columns
                null, //where
                null, //where args
                "date", //group by
                null, //having
                "date_added", //order by
                null //limit
        );

        cursor.moveToFirst();

        return cursor;
    }

    /**
     * Returns an array of strings showing the spending total of each week
     *
     * @return total spending per day
     */
    public Cursor getTotalsPerWeek() {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = null;

        String[] projection = {"(strftime('%m/%d', min(date_added)) || ' - ' || strftime('%m/%d', max(date_added))) as date", "sum(amount) as total", "date(date_added, '+1 day', 'weekday 0') as first_day"};
        cursor = db.query(
                ExpenseLogContract.EXPENSE_LOG,
                projection, //columns
                null, //where
                null, //where args
                "first_day", //group by
                null, //having
                "first_day", //order by
                null //limit
        );

        cursor.moveToFirst();

        return cursor;
    }

    /**
     * Returns an array of strings showing the spending total of each week
     *
     * @return total spending per month
     */
    public Cursor getTotalsPerMonth() {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = null;

        String[] projection = {"strftime('%m', date_added) as month", "strftime('%Y', date_added) as year", "sum(amount) as total"};
        cursor = db.query(
                ExpenseLogContract.EXPENSE_LOG,
                projection, //columns
                null, //where
                null, //where args
                "year, month", //group by
                null, //having
                "year, month", //order by
                null //limit
        );

        cursor.moveToFirst();

        return cursor;
    }

    /**
     * Get last Sunday's date
     *
     * @return date
     */
    private String getDateLastSunday() {
        Calendar calendar = Calendar.getInstance();

        // Note: first day of the week is Sunday
        // get first day of the week
        int day_of_the_week = (calendar.get(Calendar.DAY_OF_WEEK)-1) * -1;
        calendar.add(Calendar.DATE, day_of_the_week);
        String last_sunday = calendar.get(Calendar.YEAR) + "-" + String.format("%02d", calendar.get(Calendar.MONTH)+1) + "-" + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH));

        return last_sunday;
    }
}
