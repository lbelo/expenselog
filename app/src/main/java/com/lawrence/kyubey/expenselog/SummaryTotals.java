package com.lawrence.kyubey.expenselog;

import android.accessibilityservice.GestureDescription;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.lawrence.kyubey.expenselog.data.SummaryTotalEntry;
import com.lawrence.kyubey.expenselog.data.ExpenseLogDbHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class SummaryTotals extends BaseActivity implements OnClickListener {

    private ExpenseLogDbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();

        setContentView(R.layout.activity_daily_totals);

        String summary_type = getIntent().getStringExtra("summary_type");

        dbHelper = new ExpenseLogDbHelper(this);
        if (summary_type != null && summary_type.equals("weekly")) {
            weeklyTotals();
        } else if (summary_type != null && summary_type.equals("monthly")) {
            monthlyTotals();
        } else {
            // daily totals are the default
            dailyTotals();
        }
    }

    @Override
    public void onClick(View view) {
        String button_clicked = view.getTag().toString();

        Intent reloadSummary = new Intent(this, SummaryTotals.class);
        reloadSummary.putExtra("summary_type", button_clicked);

        // reload the expense list
        startActivity(reloadSummary);
    }

    /**
     * Generate a list of daily totals
     */
    private void dailyTotals() {
        Cursor daily_totals = dbHelper.getTotalsPerDay();
        ArrayList<SummaryTotalEntry> logs = new ArrayList<SummaryTotalEntry>();

        if (daily_totals.getCount() > 0) {
            do {
                String date_added = "";
                try {
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = formatter.parse(daily_totals.getString(0));

                    SimpleDateFormat output_formatter = new SimpleDateFormat("EEE - dd MMM yyyy");
                    date_added = output_formatter.format(date).toString();
                } catch (ParseException e) {
                    //fallback to unformatted date
                    date_added = daily_totals.getString(0);
                }

                float total = daily_totals.getFloat(1);

                SummaryTotalEntry dte = new SummaryTotalEntry(total, date_added);
                logs.add(dte);

            } while (daily_totals.moveToNext());
        }
        daily_totals.close();

        DailyTotalArrayAdapter daily_sums = new DailyTotalArrayAdapter(this, R.layout.daily_total_list_item, logs);

        ListView listView = (ListView) findViewById(R.id.daily_totals);
        listView.setAdapter(daily_sums);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent expenseIntent = new Intent(getBaseContext(), ExpenseListActivity.class);
                String entry_date = null;

                try {
                    String list_entry_date = ((TextView) view.findViewById(R.id.dt_date)).getText().toString();

                    SimpleDateFormat formatter = new SimpleDateFormat("EEE - dd MMMM yyyy");
                    Date date = formatter.parse(list_entry_date);

                    SimpleDateFormat output_formatter = new SimpleDateFormat("yyyy-MM-dd");
                    entry_date = output_formatter.format(date).toString();
                } catch (Exception e) {
                    //do nothing we will just show all
                }

                expenseIntent.putExtra("date", entry_date);

                startActivity(expenseIntent);
            }
        });
    }

    /**
     * Generate a list of weekly totals
     */
    private void weeklyTotals() {
        Cursor weekly_totals = dbHelper.getTotalsPerWeek();
        ArrayList<SummaryTotalEntry> logs = new ArrayList<SummaryTotalEntry>();

        if (weekly_totals.getCount() > 0) {
            do {
                String date_range = weekly_totals.getString(0);

                float total = weekly_totals.getFloat(1);

                SummaryTotalEntry dte = new SummaryTotalEntry(total, date_range);
                logs.add(dte);

            } while (weekly_totals.moveToNext());
        }
        weekly_totals.close();

        DailyTotalArrayAdapter weekly_sums = new DailyTotalArrayAdapter(this, R.layout.daily_total_list_item, logs);

        ListView listView = (ListView) findViewById(R.id.daily_totals);
        listView.setAdapter(weekly_sums);
    }

    /**
     * Generate a list of monthly totals
     */
    private void monthlyTotals() {
        Cursor monthly_totals = dbHelper.getTotalsPerMonth();
        ArrayList<SummaryTotalEntry> logs = new ArrayList<SummaryTotalEntry>();

        if (monthly_totals.getCount() > 0) {
            do {
                String[] month_names = {
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                };

                int month = Integer.parseInt(monthly_totals.getString(0)) - 1;
                String year = monthly_totals.getString(1);

                String month_range = month_names[month] + " " + year;

                float total = monthly_totals.getFloat(2);

                SummaryTotalEntry dte = new SummaryTotalEntry(total, month_range);
                logs.add(dte);

            } while (monthly_totals.moveToNext());
        }
        monthly_totals.close();

        DailyTotalArrayAdapter monthly_sums = new DailyTotalArrayAdapter(this, R.layout.daily_total_list_item, logs);

        ListView listView = (ListView) findViewById(R.id.daily_totals);
        listView.setAdapter(monthly_sums);
    }
}
