package com.lawrence.kyubey.expenselog.data;

/**
 * Created by kyubey on 6/10/17.
 */

public class SummaryTotalEntry {

    public String date;
    public float amount;

    public SummaryTotalEntry(float amount, String date) {
        this.amount = amount;
        this.date = date;
    }
}
